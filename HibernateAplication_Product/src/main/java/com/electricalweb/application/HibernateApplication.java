package com.electricalweb.application;

import com.electricalweb.daos.ProductDao;
import com.electricalweb.entities.Product;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class HibernateApplication {

    private final ProductDao productDao;
    private final BufferedReader userInputReader;

    public static void main(String[] args) throws Exception {

        EntityManager entityManager = Persistence
                .createEntityManagerFactory("product-unit")
                .createEntityManager();
        ProductDao userDao = new ProductDao(entityManager);
        BufferedReader userInputReader =
                new BufferedReader(new InputStreamReader(System.in));
        new HibernateApplication(userDao, userInputReader).run();
    }

    public HibernateApplication(ProductDao productDao, BufferedReader userInputReader) {
        this.productDao = productDao;
        this.userInputReader = userInputReader;
    }


    private void run() throws IOException {
        System.out.println("Enter an option: "
                + "1) Insert a new product. "
                + "2) Find a product. "
                + "3) Edit a product. "
                + "4) Delete a product.");
        System.out.print("pilihan : ");
        int option = Integer.parseInt(userInputReader.readLine());

        switch (option) {
            case 1:
                persistNewUser();
                break;
            case 2:
                fetchExistingUser();
                break;
            case 3:
                updateExistingUser();
                break;
            case 4:
                removeExistingUser();
                break;
        }
    }

    private void persistNewUser() throws IOException {
        String name = requestStringInput("the name of the product");
        int categoryId = Integer.parseInt(requestStringInput("the categoryId of the product"));
        double price = Double.parseDouble(requestStringInput("the price of the product"));
        productDao.persist(name, categoryId, price);
    }

    private void fetchExistingUser() throws IOException {
        int id = requestIntegerInput("the user ID");
        Product user = productDao.find(id);
        System.out.print("Name : " + user.getName() + " Category ID : " + user.getCategoryId() + "Price : " + user.getPrice());
    }

    private void updateExistingUser() throws IOException {
        int id = requestIntegerInput("the product ID");
        String name = requestStringInput("the name of new product");
        int categoryId = Integer.parseInt(requestStringInput("the categoryId of new product"));
        double price = Double.parseDouble(requestStringInput("the price of new product"));
        productDao.update(id, name, categoryId,price);
    }

    private void removeExistingUser() throws IOException {
        int id = requestIntegerInput("the product ID");
        productDao.remove(id);
    }

    private String requestStringInput(String request) throws IOException {
        System.out.printf("Enter %s: ", request);
        return userInputReader.readLine();
    }

    private int requestIntegerInput(String request) throws IOException {
        System.out.printf("Enter %s: ", request);
        return Integer.parseInt(userInputReader.readLine());
    }

}
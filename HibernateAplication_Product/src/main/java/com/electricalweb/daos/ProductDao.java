package com.electricalweb.daos;

import com.electricalweb.entities.Product;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

public class ProductDao {

    private EntityManager entityManager;
    private EntityTransaction entityTransaction;

    public ProductDao(EntityManager entityManager) {
        this.entityManager = entityManager;
        this.entityTransaction = this.entityManager.getTransaction();
    }

    public void persist(String name, int categoryId, double price) {
        beginTransaction();
        Product user = new Product(name,categoryId,price);
        entityManager.persist(user);
        commitTransaction();
    }

    public Product find(int id) {
        return entityManager.find(Product.class, id);
    }

    public void update(int id, String name, int categoryId, double price) {
        beginTransaction();
        Product user = entityManager.find(Product.class, id);
        user.setName(name);
        user.setCategoryId(categoryId);
        user.setPrice(price);
        entityManager.merge(user);
        commitTransaction();
    }

    public void remove(int id) {
        beginTransaction();
        Product user = entityManager.find(Product.class, id);
        entityManager.remove(user);
        commitTransaction();
    }

    private void beginTransaction() {
        try {
            entityTransaction.begin();
        } catch (IllegalStateException e) {
            rollbackTransaction();
        }
    }

    private void commitTransaction() {
        try {
            entityTransaction.commit();
            entityManager.close();
        } catch (IllegalStateException e) {
            rollbackTransaction();
        }
    }

    private void rollbackTransaction() {
        try {
            entityTransaction.rollback();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }
}
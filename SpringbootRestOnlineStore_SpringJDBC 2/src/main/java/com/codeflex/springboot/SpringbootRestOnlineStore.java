package com.codeflex.springboot;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication(scanBasePackages={"com.codeflex.springboot"})
public class SpringbootRestOnlineStore {
    public static void main(String[] args) {
        SpringApplication.run(SpringbootRestOnlineStore.class, args);
    }
}

//public class SpringbootRestOnlineStore  implements CommandLineRunner {
//
//    private static final Logger log = LoggerFactory.getLogger(StartApplication.class);
//
//    @Autowired
//    JdbcTemplate jdbcTemplate;
//
//    @Autowired
//    @Qualifier("jdbcProductRepository")              // Test JdbcTemplate
//    //@Qualifier("namedParameterJdbcBookRepository")  // Test NamedParameterJdbcTemplate
//    private ProductRepository productRepository;
//
//    @Bean
//    public LobHandler lobHandler() {
//        return new DefaultLobHandler();
//    }
//
//
//    public static void main(String[] args) {
//        SpringApplication.run(SpringbootRestOnlineStore.class, args);
//    }
//
//
//    @Override
//    public void run(String... args) throws Exception {
//        log.info("StartApplication...");
//
//        startProductApp();
//
//    }
//
//    void startProductApp() {
//
//        // MENU
////        1. Select Product by ID
////        2. Select All Product
////        3. Insert Product
////        4. Update Product
////        5. Delete Product
//
////        log.info("Creating tables for testing...");
////
////        jdbcTemplate.execute("DROP TABLE IF EXISTS product");
////        jdbcTemplate.execute("CREATE TABLE product(" +
////                "id SERIAL, name VARCHAR(255), price NUMERIC(15, 2))");
//
//        List<Product> products = Arrays.asList(
//                new Product("Buku Komik", 3, 2000),
//                new Product("Buku Java", 3, 3000),
//                new Product("Penggaris", 2, 2000),
//                new Product("Pulpen", 1, 1000)
//        );
//
//        log.info("[SAVE]");
//        products.forEach(product -> {
//            log.info("Saving...{}", product.getName());
//            productRepository.saveProduct(product);
//        });
//
////        // count
////        log.info("[COUNT] Total products: {}", productRepository.count());
//
//        // find all
//        log.info("[FIND_ALL] {}", productRepository.findAllProducts());
//
//        // find by id
//        log.info("[FIND_BY_ID] :2L");
//        Product product = productRepository.findById(2L).orElseThrow(IllegalArgumentException::new);
//        log.info("{}", product);
//
//        // find by name (like) and price
//        log.info("[FIND_BY_NAME_AND_PRICE] : like '%Buku%'");
//        log.info("{}", productRepository.findByName("Buku"));
////
////        // get name (string) by id
////        log.info("[GET_NAME_BY_ID] :1L = {}", productRepository.findNameById(1L));
//
//        // update
////        log.info("[UPDATE] :2L :99.99");
////        product.setPrice(5000);
////        log.info("rows affected: {}", productRepository.updateProduct(product));
//
//        // delete
////        log.info("[DELETE] :3L");
////        log.info("rows affected: {}", productRepository.deleteProductById(3L));
//
//        // find all
//        log.info("[FIND_ALL] {}", productRepository.findAllProducts());
//
//    }
//}
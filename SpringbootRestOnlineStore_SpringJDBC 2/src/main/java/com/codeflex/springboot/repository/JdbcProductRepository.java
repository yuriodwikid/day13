package com.codeflex.springboot.repository;

import com.codeflex.springboot.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository("productRepository")
public class JdbcProductRepository implements ProductRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;
//
//    @Autowired
//    LobHandler lobHandler;

//    //  Using two hashmaps in order to provide performance of O(1) while fetching products
//    private static HashMap<Long, Product> products = new HashMap<>();
//    private static HashMap<String, Long> idNameHashMap = new HashMap<>();

    public List<Product> findAllProducts() {
        return jdbcTemplate.query(
                "select * from product",
                (rs, rowNum) ->
                        new Product(
                                rs.getLong("id"),
                                rs.getString("name"),
                                rs.getInt("categoryId"),
                                rs.getDouble("price")
                        )
        );
    }

    //return object by using Optional

    public Product findById(long id) {
        return jdbcTemplate.queryForObject(
                "select * from product where id = ?",
                new Object[]{id},
                (rs, rowNum) ->
                        new Product(
                                rs.getLong("id"),
                                rs.getString("name"),
                                rs.getInt("categoryId"),
                                rs.getDouble("price")
                        )
        );
    }

    public List<Product> findByName(String name) {
        return jdbcTemplate.query(
                "select * from product where name like ?",
                new Object[]{"%" + name + "%"},
                (rs, rowNum) ->
                        new Product(
                                rs.getLong("id"),
                                rs.getString("name"),
                                rs.getInt("categoryId"),
                                rs.getDouble("price")
                        )
        );
    }

//    public Product findByName(String name) {
//        return jdbcTemplate.query(
//                "select * from product where name = ?",
//                new Object[]{name},
//                (rs, rowNum) ->
//                        new Product(
//                                rs.getLong("id"),
//                                rs.getString("name"),
//                                rs.getInt("categoryId"),
//                                rs.getDouble("price")
//                        )
//        );
//    }



    public void saveProduct(Product product) {
      int ret = jdbcTemplate.update(
                "insert into product (name, categoryId, price) values(?,?,?)",
                product.getName(), product.getCategoryId(), product.getPrice());
        System.out.println(ret + " record inserted");
    }


    public void updateProduct(Product product, long id) {
        int ret = jdbcTemplate.update(
                "update product set name = ?, categoryId = ?, price = ? where id = ?",
                product.getName(), product.getCategoryId(), product.getPrice(), product.getId());
        System.out.println(ret + " record updated");
    }

    public void deleteProductById(long id) {
        int ret = jdbcTemplate.update(
                "delete from product where id = ?",
                id);
        System.out.println(ret + " record deleted");
    }

//    public boolean isProductExist(Product product) throws SQLException, ClassNotFoundException {
//        Class.forName("com.mysql.cj.jdbc.Driver");
//
//        Connection con = DriverManager.getConnection(
//                "jdbc:mysql://localhost:3306/coba?serverTimezone=UTC","root","Jadimanfaat20");
//
//        PreparedStatement stmt = null;
//        //Query untuk mengambil semua data yang ada di db exam2 dan tabel login_exam2
//        stmt = con.prepareStatement("select * from product");
//        ResultSet rs=stmt.executeQuery();
//        //Melakukan looping jika terdapat data pada db, untuk mengecek apakah terdapat nama product yang sama
//        //dengan mengabaikan upper atau lower case
//        while(rs.next()){
//            //Jika product yang diinputkan sama dengan nama product yang ada di db
//            if (rs.getString(2).equalsIgnoreCase(product.getName())){
//                return true;
//            }
//        }
//        return false;
//    }

    public void deleteAllProducts() {
        int ret = jdbcTemplate.update(
                "delete from product");
        System.out.println(ret + " record(s) deleted");
    }

    @Override
    public boolean isProductExist(Product product) {
        return false;
    }

}
package com.codeflex.springboot.repository;


import java.util.List;

import com.codeflex.springboot.model.Product;
import org.springframework.stereotype.Repository;

@Repository("productRepository")
public interface ProductRepository {

    Product findById(long id);

    List<Product>  findByName(String name);

    void saveProduct(Product product);

    void updateProduct(Product product, long id);

    void deleteProductById(long id);

    List<Product> findAllProducts();

    void deleteAllProducts();

    boolean isProductExist(Product product);

}
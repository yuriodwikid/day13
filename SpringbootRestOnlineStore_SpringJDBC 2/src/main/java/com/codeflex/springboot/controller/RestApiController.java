package com.codeflex.springboot.controller;

import com.codeflex.springboot.model.Product;
import com.codeflex.springboot.repository.ProductRepository;
//import com.codeflex.springboot.repository.ProductServiceDB;
//import com.codeflex.springboot.repository.ProductServiceDB;
//import com.codeflex.springboot.repository.ProductServiceDatabase;
import com.codeflex.springboot.util.CustomErrorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RestApiController {

    public static final Logger logger = LoggerFactory.getLogger(RestApiController.class);

    @Autowired
    ProductRepository productRepository; //Service which will do all data retrieval/manipulation work

//    @Autowired
//    ProductServiceDB productServiceDB; //Service which will do all data retrieval/manipulation work
    @Autowired
    @Qualifier("jdbcProductRepository")

    // -------------------Retrieve All Products--------------------------------------------

    @RequestMapping(value = "/product/", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<List<Product>> listAllProducts() {
//        List<Product> products = productServiceHM.findAllProducts();
        List<Product> products = productRepository.findAllProducts();
        if (products.isEmpty()) {
            return new ResponseEntity<>(products, HttpStatus.NOT_FOUND);
        }
        productRepository.findAllProducts();
        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    // -------------------Retrieve Single Product------------------------------------------

    @RequestMapping(value = "/product/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getProduct(@PathVariable("id") long id) {
        logger.info("Fetching Product with id {}", id);
        Product product = productRepository.findById(id);
//        if (product == null) {
//            logger.error("Product with id {} not found.", id);
//            return new ResponseEntity<>(new CustomErrorType("Product with id " + id  + " not found"), HttpStatus.NOT_FOUND);
//        }
//        productServiceDB.findById(id);
        return new ResponseEntity<>(product, HttpStatus.OK);
    }

    // -------------------Retrieve Single Product By Name------------------------------------------

    @RequestMapping(value = "/product/:{name}", method = RequestMethod.GET)
    public ResponseEntity<List<?>>  getProduct(@PathVariable("name") String name) {
        logger.info("Fetching Product with name {}", name);
        List<Product> product = productRepository.findByName(name);
//        if (product == null) {
//            logger.error("Product with id {} not found.", id);
//            return new ResponseEntity<>(new CustomErrorType("Product with id " + id  + " not found"), HttpStatus.NOT_FOUND);
//        }
//        productServiceDB.findById(id);
        return new ResponseEntity<>(product, HttpStatus.OK);
    }

    // -------------------Create a Product-------------------------------------------

    @RequestMapping(value = "/product/", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity<?> createProduct(@RequestBody Product product) {
        logger.info("Creating Product : {}", product);
        System.out.println("Creating Product");
//        if (productServiceHM.isProductExist(product)) {
//            logger.error("Unable to create. A Product with name {} already exist", product.getName());
//            return new ResponseEntity<>(new CustomErrorType("Unable to create. A Product with name " +
//                    product.getName() + " already exist."), HttpStatus.CONFLICT);
//        }
//        productServiceHM.saveProduct(product);

        productRepository.saveProduct(product);

        return new ResponseEntity<>(product, HttpStatus.CREATED);
    }

    // ------------------- Update a Product ------------------------------------------------

    @RequestMapping(value = "/product/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateProduct(@PathVariable("id") long id, @RequestBody Product product) {
        logger.info("Updating Product with id {}", id);

        Product currentProduct = productRepository.findById(id);

//        if (currentProduct == null) {
//            logger.error("Unable to update. Product with id {} not found.", id);
//            return new ResponseEntity<>(new CustomErrorType("Unable to update. Product with id " + id + " not found."),
//                    HttpStatus.NOT_FOUND);
//        }

        currentProduct.setName(product.getName());
        currentProduct.setCategoryId(product.getCategoryId());
        currentProduct.setPrice(product.getPrice());
        currentProduct.setId(id);

        productRepository.updateProduct(currentProduct, id);
//        productServiceHM.updateProduct(currentProduct);
        return new ResponseEntity<>(currentProduct, HttpStatus.OK);
    }

    // ------------------- Delete a Product-----------------------------------------

    @RequestMapping(value = "/product/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteProduct(@PathVariable("id") long id) {
        logger.info("Fetching & Deleting Product with id {}", id);

        Product product = productRepository.findById(id);
//        Product product = productServiceHM.findById(id);
//        if (product == null) {
//            logger.error("Unable to delete. Product with id {} not found.", id);
//            return new ResponseEntity<>(new CustomErrorType("Unable to delete. Product with id " + id + " not found."),
//                    HttpStatus.NOT_FOUND);
//        }
        productRepository.deleteProductById(id);
//        productServiceHM.deleteProductById(id);
        return new ResponseEntity<Product>(HttpStatus.NO_CONTENT);
    }

    // ------------------- Delete All Products-----------------------------

    @RequestMapping(value = "/product/", method = RequestMethod.DELETE)
    public ResponseEntity<Product> deleteAllProducts() {
        logger.info("Deleting All Products");

        productRepository.deleteAllProducts();
//        productServiceHM.deleteAllProducts();
        return new ResponseEntity<Product>(HttpStatus.NO_CONTENT);
    }

}
package com.codeflex.springboot.controller;

import com.codeflex.springboot.repo.ProductRepo;
import com.codeflex.springboot.entity.Product;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;

@RestController
@RequestMapping("/api")
public class RestApiProductController {

    public static final Logger logger = LoggerFactory.getLogger(RestApiProductController.class);

    @Autowired
    private ProductRepo productRepo;


    // -------------------Select a Product-------------------------------------------
    // Save to DB - Insert to Database
    @RequestMapping(value = "/productrepo/{id}", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<?> selectProductNew(@PathVariable int id,@RequestBody Product product) throws SQLException, ClassNotFoundException {
        logger.info("Creating Product : {}", id);

        Product currentProduct = productRepo.findById(id).get();

            currentProduct.getId();
            currentProduct.getName();
            currentProduct.getCategoryId();
            currentProduct.getPrice();

            productRepo.getClass();

            return new ResponseEntity<>(currentProduct, HttpStatus.OK);

    }

    // -------------------Select a Product-------------------------------------------
    // Save to DB - Insert to Database
    @RequestMapping(value = "/productrepo/", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<List<Product>> selectAllProductNew() {

        List<Product> productss;
        productss=productRepo.findAll();

        return new ResponseEntity<>(productss,HttpStatus.OK);

    }

    // -------------------Create a Product-------------------------------------------
    // Save to DB - Insert to Database
    @RequestMapping(value = "/productrepo/", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity<?> createProductNew(@RequestBody Product product) throws SQLException, ClassNotFoundException {
        logger.info("Creating Product : {}", product);

        productRepo.save(product);

        return new ResponseEntity<>(product, HttpStatus.CREATED);
    }

    // -------------------update a Product-------------------------------------------
    // Save to DB - Insert to Database
    @RequestMapping(value = "/productrepo/{id}", method = RequestMethod.PUT, produces="application/json")
    public ResponseEntity<?> updateProductNew(@PathVariable("id") int id,@RequestBody Product product) throws SQLException, ClassNotFoundException {
        logger.info("updated Product : {}", id);

        Product currentProduct = productRepo.findById(id).get();
        currentProduct.setName(product.getName());
        currentProduct.setCategoryId(product.getCategoryId());
        currentProduct.setPrice(product.getPrice());

        productRepo.save(currentProduct);

        return new ResponseEntity<>(currentProduct, HttpStatus.OK);
    }

    // -------------------delete a Product-------------------------------------------
    // Save to DB - Insert to Database
    @RequestMapping(value = "/productrepo/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteProduct(@PathVariable("id") int id) throws SQLException, ClassNotFoundException {
        logger.info("deleted Product : {}", id);

        Product currentProduct = productRepo.findById(id).get();

        productRepo.delete(currentProduct);


        return new ResponseEntity<>(currentProduct, HttpStatus.NO_CONTENT);
    }

    // -------------------delete all Product-------------------------------------------
    // Save to DB - Insert to Database
    @RequestMapping(value = "/productrepo/", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteAllProduct(){
        logger.info("deleted Product");

        productRepo.deleteAll();


        return new ResponseEntity<Product>(HttpStatus.NO_CONTENT);
    }

}